---
layout: post
title:  "Extend a Virtual Disk on Linux"
date:   2020-03-26
categories: [linux]
tags: [basics, storage]
author: fxs
---

Back to Basics, Storage, Extend a Virtual Disk on Linux
=======================================================

This post covers the tasks required on a Linux host and does not account for the hypervisor used, please research the hypervisor documentation if expansion/addition of a virtual disk is not self explanatory on the used hypervisor. The examples are from RHEL/CentOS and may differ on other distributions.

**Warning!** Before making any changes, be sure you have researched what you need to do. You might lose data if you are not sure what you're doing. Take a backup if possible. I take no responsibility for data that you might have lost.

Below are two common methods for extending virtual disks.

Method A: Extend an already used disk
-------------------------------------

*Note! Works with either LVM or raw disks*

The general gist of what needs to happen:

1. Increase the virtual disk size
    * Make sure that the OS is aware of the changed disk size
3. Resize the (last) partition on the disk
4. If used, resize the LVM stack:
	1. Extend the physical volume
	2. Extend the logical volume
5. Resize the filesystem

In this example we are going to extend a raw disk "vdb" and extend the partition as well as the file system.

Increase the virtual disk size
------------------------------

This will be done in the hypervisor, you can check for the disk size with `lsblk`.
```plaintext
[fxs@rhel8 ~]$ lsblk /dev/vdb
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vdb    252:16   0   4G  0 disk 
`-vdb1 252:17   0   2G  0 part /mnt/data
```

Make sure that the OS is aware of the changed disk size.

If the disk size has not increased after disk resize, you need to either manually make sure that kernel receives the information, or simply reboot. To make sure disk changes have been processed, you can run `udevadm settle`. All kernel changes are logged to dmesg, which can be browsed easily with f.ex. `dmesg --human` command.

You can also init a rescan for the disk, but you need to know the iSCSI host address.
```plaintext
echo 1 > /sys/class/scsi_device/0\:0\:0\:0/device/rescan
```

Resize the (last) partition on the disk
---------------------------------------

After increasing the disk size you can proceed to extend the partition. This can be done with f.ex. parted, fdisk, or gdisk. Note that you can only extend the last partition.

In the following example I have used parted:
```plaintext
[fxs@rhel8 ~]$ sudo parted /dev/vdb
GNU Parted 3.2
Using /dev/vdb
Welcome to GNU Parted! Type 'help' to view a list of commands.
(parted) print
Warning: Not all of the space available to /dev/vdb appears to be used, you can fix the GPT to use all of the space (an
extra 4194304 blocks) or continue with the current setting?
Fix/Ignore? fix
Model: Virtio Block Device (virtblk)
Disk /dev/vdb: 4295MB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start   End     Size    File system  Name  Flags
 1      1049kB  2147MB  2146MB  xfs          data

(parted) resizepart
Partition number? 1
Warning: Partition /dev/vdb1 is being used. Are you sure you want to continue?
Yes/No? yes
End?  [2147MB]? 4295MB
(parted) quit

[fxs@rhel8 ~]$ sudo udevadm settle
[fxs@rhel8 ~]$ lsblk /dev/vdb
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vdb    252:16   0   4G  0 disk 
`-vdb1 252:17   0   4G  0 part /mnt/data
```

**Note!** At this point, if you are running LVM, you need to run `pvresize` and `lvextend /dev/<vgname>/<lvname> -l +100%FREE`.

Resize the filesystem
---------------------

After extending the partition, you can proceed to extend the file system. Resize the file system with `xfs_growfs` command. In case you are using ext4, you need to use `resize2fs` command. On other file systems you'll need some other command.
```plaintext
[fxs@rhel8 ~]$ df -hT /dev/vdb1
Filesystem     Type  Size  Used Avail Use% Mounted on
/dev/vdb1      xfs   2.0G   47M  2.0G   3% /mnt/data

[fxs@rhel8 ~]$ sudo xfs_growfs /mnt/data/
meta-data=/dev/vdb1              isize=512    agcount=4, agsize=131007 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=524027, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 524027 to 1048315
```

And complete.


Method B: Add a new virtual disk
--------------------------------

*Note! Works with LVM*

With this method you need to:

1. Add a new virtual disk
    * Check that the OS is aware of the new disk
2. OPTIONAL: Partition the disk
3. Add the disk as a physical volume to LVM stack
4. Add the disk to a volume group
5. Extend the logical volume
6. Resize the file system

In this example we are going tho extend the LVM managed file system by adding a disk, making it a physical volume, adding it to volume group and the extending the wanted logical volume, and at last extending the file system.

Add a new virtual disk
----------------------

This will be done in the hypervisor, you can check for the new disk with `lsblk`.
```plaintext
[fxs@rhel8 ~]$ lsblk
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0            11:0    1 1024M  0 rom  
vda           252:0    0   20G  0 disk 
|-vda1        252:1    0    1G  0 part /boot
`-vda2        252:2    0   19G  0 part 
  |-rhel-root 253:0    0   17G  0 lvm  /
  `-rhel-swap 253:1    0    2G  0 lvm  [SWAP]
vdb           252:16   0    5G  0 disk 
```

Note the added disk "vdb".

If the disk is not showing up, you need to either try to scan for disk changes or reboot.

**scan_scsi.sh**
```bash
#!/usr/bin/env bash
# SCAN ALL SCSI HOSTS FOR NEW DISKS

# Set variable to right path
scsi_host='/sys/class/scsi_host'

# Scan through all the present scsi hosts
for i in `ls $scsi_host`;
do
echo "- - -" > ${scsi_host}/${i}/scan
done
```
You might want to partition the disk, but this is optional. It might be a good idea in case you want to make sure all tools know that the disk is in use, as some might not recognize the LVM.

Add the disk as a physical volume to LVM stack
----------------------------------------------

After adding the disk and optionally partitioning the disk, you can add the disk (or partition) as a physical volume.
```plaintext
[fxs@rhel8 ~]$ sudo pvcreate /dev/vdb
  Physical volume "/dev/vdb" successfully created.
```

Add the disk to a volume group
------------------------------

Then proceed to add it to an existing volume group, in this case "rhel".
```plaintext
[fxs@rhel8 ~]$ sudo vgextend rhel /dev/vdb
  Volume group "rhel" successfully extended
```

Extend the logical volume
-------------------------

Add the added space to the wanted logical volume, in this case "root" (full path being `/dev/rhel/root` or `/dev/mapper/rhel-root`, both should work)
```plaintext
[fxs@rhel8 ~]$ sudo lvextend /dev/rhel/root -l +100%FREE
  Size of logical volume rhel/root changed from <17.00 GiB (4351 extents) to 21.99 GiB (5630 extents).
  Logical volume rhel/root successfully resized.
```

Resize the file system
----------------------

Proceed to resize the file system with `xfs_growfs` command. In case you are using ext4 file system, you need to use `resize2fs` command. On other file systems you'll need some other command.

```plaintext
[fxs@rhel8 ~]$ sudo xfs_growfs /
meta-data=/dev/mapper/rhel-root  isize=512    agcount=4, agsize=1113856 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=4455424, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 4455424 to 5765120
```

Conclusion
----------

We went through with a couple common methods of extending disks, there might be other ways but the general steps are usually the same. By no means use this guide blindly, research properly and choose the appropriate way to extend your disks.

If you are dealing with encrypted disks, then this guide will only guide you partway and you'd have to use `cryptsetup resize` or a similar command depending on your setup, but this post is not going to cover that at this time.

Comprehensive documentation about managing storage can be found f.ex. on [Red Hat site](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/managing_storage_devices/index). When setting up a new server, consider using the all new "stratis" implementation.

br,
fxs
