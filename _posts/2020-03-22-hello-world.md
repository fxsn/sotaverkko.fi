---
layout: post
title:  "Hello, World!"
date:   2020-03-22
categories: [general]
author: fxs
---

Hello, World!
========

This blog is a source of information 'nuggets'. You'll find both informative write ups, as well as some personal musings, mostly considering Linux but also some more generic IT topics.

Code Notation Example
=============

{% highlight bash %}
#!/bin/env bash
date -R
echo "File types in target directory:"    
echo "-------------------------------"    

cd $1
for file in $(ls)
do
        file $file
done
{% endhighlight %}

Quotation Example:
==================

> "Talk is cheap. Show me the code." [lkml.org][1]

> "Microsoft isn't evil, they just make really crappy operating systems." [twitter.com][2]

> "Intelligence is the ability to avoid doing work, yet getting the work done." [twitter.com][3]

<div style="text-align: right"> ― Linus Torvalds </div>

[1]: https://lkml.org/lkml/2000/8/25/132
[2]: https://twitter.com/linus__torvalds/status/296333371393597440
[3]: https://twitter.com/linus__torvalds/status/296333706828849153

br,
fxs
